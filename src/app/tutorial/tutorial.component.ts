import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss']
})
export class TutorialComponent implements OnInit {

  steps = [
    {step:1, name:"Click", description:"Click start button", icon:"https://image.flaticon.com/icons/svg/716/716427.svg"},
    {step:2, name:"Submit photo", description:"Submit one photo of your face", icon:"https://image.flaticon.com/icons/svg/685/685686.svg"},
    {step:3, name:"Loading", description:"Look important events on earth", icon:"https://image.flaticon.com/icons/svg/811/811173.svg"},
    {step:4, name:"Result", description:"Your mosaic is done!", icon:"https://image.flaticon.com/icons/svg/1055/1055671.svg"}
  ]
  constructor() { }

  ngOnInit() {
  }

}
