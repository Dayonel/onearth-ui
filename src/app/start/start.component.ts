import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {
  public loadingState = '';
  static loadingState: any;
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  onSubmitChange() {
    this.loadingState = 'is-open';
    setTimeout(() => {
      this.notify.emit('is-closed');
    }, 12000);
  }

  ngOnInit() {
  }

}
