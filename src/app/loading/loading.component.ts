import { Component, OnInit } from '@angular/core';
import { interval, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  name$: Observable<any>;
  description$: Observable<any>;
  icon$: Observable<any>;
  dataImages = [
    {id:1, name:"Staring Down Hurricane Florence", description:"'Ever stared down the gaping eye of a category 4 hurricane? It's chilling, even from space,' says European Space Agency astronaut Alexander Gerst (@Astro_Alex).", icon:"https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/churning.jpeg"},
    {id:2, name:"Aurora and Manicouagan Crater", description:"An astronaut aboard the International Space Station adjusted the camera for night imaging and captured the green veils and curtains of an aurora that spanned thousands of kilometers over Quebec, Canada.", icon:"https://www.nasa.gov/sites/default/files/styles/image_card_4x3_ratio/public/thumbnails/image/25937026477_4b7949e87d_o.jpg"},
    {id:3, name:"The snow-capped tips of the Andes Mountains", description:"The snow-capped tips of the Andes Mountains range separates the South American countries of Chile and Argentina.", icon:"https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/iss055e086530.jpg"}
  ]

  constructor() { 
  }

  ngOnInit() {
    this.name$ = interval(3000).pipe(map(i => Object.values(this.dataImages[i])[1] ));
    this.description$ = interval(3000).pipe(map(i => Object.values(this.dataImages[i])[2] ));
    this.icon$ = interval(3000).pipe(map(i => Object.values(this.dataImages[i])[3] ));
  }
}
