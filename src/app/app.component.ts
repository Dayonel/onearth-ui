import { Component } from '@angular/core';
import { StartComponent } from './start/start.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Know yourself on earth';
  loadingClass = '';
  resultClass = 'is-closed';

  onNotify(message:string):void {
    this.loadingClass = message;
    this.resultClass = '';
  }
}
